package imgo

import "net"

type IManagement interface {
	// Parse Message 的解析对象
	Parse(From *net.Conn, OriginMsg *[]byte, flag int) (To []*net.Conn, Msg *[]byte)
	// NewConnInterceptor 当一个新连接进来时的拦截方法
	NewConnInterceptor(conn *net.Conn) *net.Conn
	// NewWorkerInterceptor 当一个worker被新创建时的拦截方法
	NewWorkerInterceptor(worker *WorkRoutine) *WorkRoutine
	// ConnCloser 当一个链接推出前最后的执行方法
	//被携程创建 注意recover()以保证主程序安全
	ConnCloser(conn *net.Conn)
}

type defaultIManagement struct {
}

func NewDefaultIManagement() *defaultIManagement {
	return &defaultIManagement{}
}

func (d *defaultIManagement) Parse(From *net.Conn, OriginMsg *[]byte, flag int) (To []*net.Conn, Msg *[]byte) {
	//可以根据flag进行不同的操作
	if flag != 0 {
		return nil, nil
	}
	//此处假装解析出来的结果就是原地址 解析出来的消息就是原消息
	To = make([]*net.Conn, 1)
	To[0] = From
	Msg = OriginMsg
	return
}

func (d *defaultIManagement) NewConnInterceptor(conn *net.Conn) *net.Conn {
	Logger.Debug().Println((*conn).RemoteAddr().String(), " in ")
	return conn
}

func (d *defaultIManagement) NewWorkerInterceptor(worker *WorkRoutine) *WorkRoutine {
	return worker
}

func (d *defaultIManagement) ConnCloser(conn *net.Conn) {
	defer func() {
		err := recover()
		if err != nil {
			Logger.Err().Println(err)
		}
	}()

	Logger.Debug().Println((*conn).RemoteAddr().String(), " out ")
	err := (*conn).Close()
	if err != nil {
		Logger.Err().Println(err.Error())
	}
	conn = nil
}
