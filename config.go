package imgo

type Configuration struct {
	GlobalMessageChannelLength int64
	MonitorPrintDelay          int64
	LogToFile                  bool
	LogFilePath                string
}
