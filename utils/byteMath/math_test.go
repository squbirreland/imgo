package byteMath

import (
	"reflect"
	"testing"
)

func TestStr2ByteUnChange(t *testing.T) {
	type args struct {
		str        string
		originBase int
	}
	tests := []struct {
		name    string
		args    args
		want    byte
		wantErr bool
	}{
		{
			"",
			args{
				"1a",
				16,
			},
			0x1a,
			false,
		},
		{
			"",
			args{
				"11010",
				2,
			},
			0x1a,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Str2Byte(tt.args.str, tt.args.originBase)
			if (err != nil) != tt.wantErr {
				t.Errorf("Str2Byte() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Str2Byte() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestByte2StrUnChange(t *testing.T) {
	type args struct {
		b          byte
		targetBase int
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			"",
			args{
				0x1a,
				2,
			},
			"00011010",
			false,
		},
		{
			"",
			args{
				0x1a,
				16,
			},
			"1a",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Byte2Str(tt.args.b, tt.args.targetBase, 8)
			if (err != nil) != tt.wantErr {
				t.Errorf("Byte2Str() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Byte2Str() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestByteArr2Int(t *testing.T) {
	type args struct {
		b []byte
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			"",
			args{
				[]byte{0xaa, 0xff},
			},
			43775,
			false,
		},
		{
			"",
			args{
				[]byte{0x00, 0x01},
			},
			1,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ByteArr2Int(tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("ByteArr2Int() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ByteArr2Int() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInt2ByteArr(t *testing.T) {
	type args struct {
		i      int
		tarBit int
	}
	var r = []byte{0x03, 0xe8}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			"",
			args{
				1000,
				2,
			},
			r,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, b := Int2ByteArr(tt.args.i, tt.args.tarBit); !reflect.DeepEqual(got, tt.want) || b {
				t.Errorf("Int2ByteArr() = %v, want %v", got, tt.want)
			}
		})
	}
}
