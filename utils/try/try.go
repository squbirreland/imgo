package try

import (
	"io"
	"log"
)

// Throw 尝试抛出一个异常
func Throw(e error) {
	if e != nil {
		log.Fatal(e.Error())
	}
}

// Catch 尝试捕获一个异常
func Catch(e error) {
	if e != nil {
		log.Println(e.Error())
	}
}

// Close 尝试关闭一件事物 前提是该事物的关闭实现了Closer接口
func Close(closer io.Closer) {
	err := closer.Close()
	Throw(err)
}
