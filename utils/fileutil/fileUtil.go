package fileutil

import (
	"fmt"
	"log"
	"os"
)

func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

// Mkdir 创建文件夹
func Mkdir(dir string) error {
	exist, err := PathExists(dir)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		if exist {
			fmt.Println(dir + "-dir already exists !")
		} else {
			// 文件夹名称，权限
			return os.Mkdir(dir, os.ModePerm)
		}
	}
	return nil
}

// RemoveDir 删除文件
func RemoveDir(dir string) error {
	exist, err := PathExists(dir)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		if exist {
			// os.RemoveAll 是遍历删除，文件夹及文件均可使用
			return os.RemoveAll(dir)
		}
	}
	return nil
}

// MkFile 创建文件
func MkFile(filePath string) error {
	exists, err1 := PathExists(filePath)
	if err1 != nil {
		return err1
	}
	if !exists {
		file, err2 := os.OpenFile(filePath, os.O_CREATE, os.ModePerm)
		// 关闭文件
		defer func(file *os.File) {
			err := file.Close()
			if err != nil {
				log.Println(err.Error())
			}
		}(file)
		return err2
	}
	return nil
}
