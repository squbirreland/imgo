# im-go

#### 介绍

    go实现的socket服务器 
    
    go+c实现的socket客户端

#### 软件架构

    软件架构说明

![img.png](resource/img.png)
MessageServer -0.0.3
![img.png](resource/img_3.png)

#### 安装教程

    设置编译环境
        SET CGO_ENABLED=0
        SET CGO_ENABLED=1
        SET GOOS=linux
        SET GOOS=windows
        SET GOOS=darwin
    编译
        go build -o ./target/ ./src/server/main.go
        go build -o ./target/ ./src/client/main.go

#### 使用说明

    -
        server/routine.go 和 server/parsing.go 文件中
        有所需要的接口说明与其默认实现

```go
package main

import (
	"gitee.com/squbirreland/imgo"
)

func main() {
	//创建管理者与配置
	c1 := imgo.NewCenter(16)
	c2 := imgo.NewCenter(16)
	//默认的实现类
	dm := imgo.NewDefaultIManagement()
	dwr := imgo.NewDefaultIWorkRoutine(1000, true)
	//启动
	imgo.Listen(8713).Run(dm, dwr, c1, c2)
	for {
		select {}
	}

	//或者使用自定义配置启动
	conf := imgo.Configuration{
		GlobalMessageChannelLength: 1024,
		LogToFile:                  true,
		LogFilePath:                "/usr/local/im-go/log",
	}
	imgo.Listen(8713).Config(&conf).Run(dm, dwr, c1, c2)
}
```

#### 日志

    -0.0.3
    优化结构
    
    主函数
        等待链接携程
        全局消息队列处理携程
    中心函数
        等待删除携程
        全局信息捕获携程
    链接对应
        消息监听携程
        消息发送携程
    临时携程
        全局消息队列-临时消息处理携程 一个消息对应一个
        新建链接携程 一个链接新建对应一个
    
    意为
    当前架构静态状况下 一个链接对应2个额外携程
    最差情况下(如大量http请求) 一个请求对应4个额外携程并且会造成转发压力

    -0.2.0
    整体大幅度重构优化
    
    重构
        目前将每个链接对应的 消息监听和发送携程
        变更为接口 可由外部实现并自定义 也可以使用默认实现
        
        也将消息解析器作为接口独立了出来 并且带上默认实现

    优化
        将链接对应的携程 与 相关处理操作
        进行了加锁 和 recover()捕获异常处理

        以保障意料内的异常不会导致服务停止与崩溃

#### 支持

    - mqtt 
        https://mcxiaoke.gitbooks.io/mqtt-cn/content/