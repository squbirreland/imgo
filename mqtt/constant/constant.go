package constant

// RightProtocolName 协议名 0 4 'M' 'Q' 'T' 'T'
var RightProtocolName = [6]byte{0x00, 0x04, 0x4d, 0x51, 0x54, 0x54}
var RightProtocolLevel byte = 0x04

// ControlBit4 link Packet.Control 控制报文
type ControlBit4 int

const (
	// Reserved -X- 0 保留
	Reserved ControlBit4 = iota
	// CONNECT C->S 1 客户端请求连接服务端
	CONNECT
	// CONNACK S->C 2 连接报文确认
	CONNACK
	// PUBLISH <-> 3 发布消息
	PUBLISH
	// PUBACK <-> 4 QoS 1 消息发布收到确认
	PUBACK
	// PUBREC <-> 5 发布收到（保证交付第一步）
	PUBREC
	// PUBREL <-> 6 发布释放（保证交付第二步 ）
	PUBREL
	// PUBCOMP <-> 7 QoS 2 消息发布完成（保证交互第三步）
	PUBCOMP
	// SUBSCRIBE C->S 8 客户端订阅请求
	SUBSCRIBE
	// SUBACK S->C 9 订阅请求报文确认
	SUBACK
	// UNSUBSCRIBE C->S 10 客户端取消订阅请求
	UNSUBSCRIBE
	// UNSUBACK S->C 11 取消订阅报文确认
	UNSUBACK
	// PINGREQ C->S 12 心跳请求
	PINGREQ
	// PINGRESP S->C 13 心跳响应
	PINGRESP
	// DISCONNECT C->S 14 客户端断开连接
	DISCONNECT
	// Reserved2 -X- 15 保留
	Reserved2       = 15
	ControlBitRight = CONNECT | CONNACK | PUBLISH | PUBACK | PUBREC | PUBREL | PUBCOMP | SUBSCRIBE | SUBACK | UNSUBSCRIBE | UNSUBACK | PINGREQ | PINGRESP | DISCONNECT
	// NeedVariableHeader  if qos >0 need publish
	NeedVariableHeader = PUBLISH | PUBACK | PUBREC | PUBREL | PUBCOMP | SUBSCRIBE | SUBACK | UNSUBSCRIBE | UNSUBACK
	// NeedPayLoad  publish is not must need
	NeedPayLoad = CONNECT | PUBLISH | SUBSCRIBE | SUBACK | UNSUBSCRIBE
)

// ZoneBit4 link Packet.Zone
type ZoneBit4 struct {
	//重复标志 1000
	DUP bool
	//服务等级 0110
	QOS QOSBit2
	//保存标志 0001
	RETAIN bool
}

// QOSBit2 .
type QOSBit2 int

const (
	OnlySendOnce QOSBit2 = iota
	OnceLessArrive
	OnlyOneArrive
	QosRight = OnlySendOnce | OnceLessArrive | OnlyOneArrive
)
