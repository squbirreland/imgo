package payload

import (
	"gitee.com/squbirreland/imgo/mqtt/agreement/constantHeader"
	"gitee.com/squbirreland/imgo/mqtt/agreement/variableHeader"
)

type IPayload interface {
	CreateBytes(cHeader constantHeader.ConstantHeader, vHeader variableHeader.IVariableHeader) error
	Parse(cHeader constantHeader.ConstantHeader, vHeader variableHeader.IVariableHeader) error
	GetPayloadBytes() []byte
	SetPayloadBytes(b []byte)
}

type DefaultPayload struct {
	payloadLength int
	payloadBytes  []byte
}

func (d *DefaultPayload) CreateBytes(cHeader constantHeader.ConstantHeader, vHeader variableHeader.IVariableHeader) error {
	return nil
}
func (d *DefaultPayload) Parse(cHeader constantHeader.ConstantHeader, vHeader variableHeader.IVariableHeader) error {
	return nil
}
func (d *DefaultPayload) GetPayloadBytes() []byte {
	return d.payloadBytes
}
func (d *DefaultPayload) SetPayloadBytes(b []byte) {
	d.payloadBytes = b
}
