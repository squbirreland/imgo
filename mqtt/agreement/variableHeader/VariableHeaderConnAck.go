package variableHeader

import (
	"errors"
	"gitee.com/squbirreland/imgo/mqtt/agreement/constantHeader"
)

// ConnAck VariableHeaderConnAck 链接确认可变头
type ConnAck struct {
	//ConnectAcknowledgeFlags 连接确认标志 @link SessionPresent
	ConnectAcknowledgeFlags SessionPresent
	//ConnectReturnCode 链接返回码 @link ConnectReturnType
	ConnectReturnCode ConnectReturnType
	//
	headerLength int
	headerBytes  []byte
}

type SessionPresent byte

const (
	// SessionUnSave 服务未保存session
	SessionUnSave SessionPresent = 0x00
	// SessionSaved 服务已保存session
	SessionSaved SessionPresent = 0x01
)

type ConnectReturnType byte

const (
	// ConnAccept 允许链接
	ConnAccept ConnectReturnType = 0x00
	// NonsupportProtocolLevel 不支持的协议版本
	NonsupportProtocolLevel ConnectReturnType = 0x01
	// NonsupportProtocolName 不支持的协议名
	NonsupportProtocolName ConnectReturnType = 0x02
	// ServerUnavailable 服务不可用
	ServerUnavailable ConnectReturnType = 0x03
	// IncorrectUsernameOrPassword 用户名密码错误
	IncorrectUsernameOrPassword ConnectReturnType = 0x04
	// PermissionDenied 无权访问
	PermissionDenied ConnectReturnType = 0x05
)

func (c *ConnAck) CreateBytes(cHeader constantHeader.ConstantHeader) error {
	b := make([]byte, 0, 4)
	b = append(b, byte(c.ConnectAcknowledgeFlags), byte(c.ConnectReturnCode))
	c.headerBytes = b
	return nil
}

func (c *ConnAck) Parse(cHeader constantHeader.ConstantHeader) error {
	return errors.New(" this func should not to be used ! ")
}

func (c *ConnAck) GetLength() int {
	return c.headerLength
}

func (c *ConnAck) SetLength(i int) {
	c.headerLength = i
}

func (c *ConnAck) SetHeaderBytes(b []byte) {
	c.headerBytes = b
}

func (c *ConnAck) GetHeaderBytes() []byte {
	return c.headerBytes
}
