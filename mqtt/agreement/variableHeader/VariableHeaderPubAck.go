package variableHeader

import (
	"errors"
	"gitee.com/squbirreland/imgo/mqtt/agreement/constantHeader"
	"gitee.com/squbirreland/imgo/utils/byteMath"
)

// PubAck VariableHeaderPuBack 回应发布确认可变头
type PubAck struct {
	Identifier PacketIdentifier
	//
	headerLength int
	headerBytes  []byte
}

func (p *PubAck) CreateBytes(cHeader constantHeader.ConstantHeader) error {
	b, bl := byteMath.Int2ByteArr(int(p.Identifier), 2)
	if !bl {
		return errors.New(" int transform byte error ")
	}
	p.headerBytes = b
	return nil
}

func (p *PubAck) Parse(cHeader constantHeader.ConstantHeader) error {
	return nil
}

func (p *PubAck) GetLength() int {
	return p.headerLength
}

func (p *PubAck) GetHeaderBytes() []byte {
	return p.headerBytes
}

func (p *PubAck) SetLength(i int) {
	p.headerLength = i
}

func (p *PubAck) SetHeaderBytes(b []byte) {
	p.headerBytes = b
}
