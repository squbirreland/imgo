package variableHeader

import "gitee.com/squbirreland/imgo/mqtt/agreement/constantHeader"

type Subscribe struct {

	//TODO
	headerLength int
	headerBytes  []byte
}

func (s *Subscribe) CreateBytes(cHeader constantHeader.ConstantHeader) error {
	return nil
}
func (s *Subscribe) Parse(cHeader constantHeader.ConstantHeader) error {
	return nil
}
func (s *Subscribe) GetLength() int {
	return s.headerLength
}
func (s *Subscribe) SetLength(i int) {
	s.headerLength = i
}
func (s *Subscribe) GetHeaderBytes() []byte {
	return s.headerBytes
}
func (s *Subscribe) SetHeaderBytes(b []byte) {
	s.headerBytes = b
}
