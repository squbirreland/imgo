package variableHeader

import (
	"gitee.com/squbirreland/imgo/mqtt/agreement/constantHeader"
)

// PacketIdentifier 报文标识符
type PacketIdentifier int

type IVariableHeader interface {
	CreateBytes(cHeader constantHeader.ConstantHeader) error
	Parse(cHeader constantHeader.ConstantHeader) error
	GetLength() int
	SetLength(i int)
	GetHeaderBytes() []byte
	SetHeaderBytes(b []byte)
}
