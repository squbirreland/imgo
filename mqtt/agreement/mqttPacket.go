package agreement

import (
	"gitee.com/squbirreland/imgo/mqtt/agreement/constantHeader"
	"gitee.com/squbirreland/imgo/mqtt/agreement/payload"
	"gitee.com/squbirreland/imgo/mqtt/agreement/variableHeader"
)

// Packet MQTT的消息解析成果
type Packet struct {
	//--固定报头 ConstantHeader
	ConstantHeader constantHeader.ConstantHeader

	//--可变报头
	VariableHeader variableHeader.IVariableHeader

	//--有效载荷 Payload
	Payload payload.IPayload
}

func NewPacket(constant constantHeader.ConstantHeader, payload payload.IPayload) *Packet {
	return &Packet{ConstantHeader: constant, Payload: payload}
}
