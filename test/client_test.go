package test

import (
	"fmt"
	"gitee.com/squbirreland/imgo/client"
	"testing"
	"time"
)

var i = 0

func TestClient(t *testing.T) {

	for i < 2 {
		go run()
		time.Sleep(10 * time.Nanosecond)
		i++
		fmt.Println(i)
	}
	fmt.Println(" create finished ")
	for {
		time.Sleep(1 * time.Second)
	}
}

func run() {
	client.NewClient("127.0.0.1", 8713).Start()
}
