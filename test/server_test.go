package test

import (
	"gitee.com/squbirreland/imgo"
	"testing"
)

func TestServer(t *testing.T) {
	//创建管理者与配置
	c1 := imgo.NewCenter(16)
	c2 := imgo.NewCenter(16)
	//默认的实现类
	dm := imgo.NewDefaultIManagement()
	dwr := imgo.NewDefaultIWorkRoutine(1000, true)
	//启动
	imgo.Listen(8713).Run(dm, dwr, c1, c2)
	for {
		select {}
	}
}
