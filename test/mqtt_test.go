package test

import (
	"gitee.com/squbirreland/imgo"
	"gitee.com/squbirreland/imgo/mqtt"
	"testing"
)

func TestMqtt(t *testing.T) {
	//创建管理者与配置
	c := imgo.NewCenter(16)
	//默认的实现类
	m := mqtt.NewMqttManagement()
	wr := mqtt.NewMqttIWorkRoutine(60*1000, true)
	//启动
	imgo.Listen(8713).Run(m, wr, c)
	for {
		select {}
	}
}
